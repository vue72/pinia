import { defineStore } from "pinia";
import useProfileStore from "./profile";
import useMessagesStore from "./messages";

export default defineStore("contacts", {
  state: () => ({
    contacts: [
      { id: 2, name: "Jason", avatar: "/avatars/avatar-02.jpg" },
      { id: 3, name: "Janet", avatar: "/avatars/avatar-03.jpg" },
    ],
  }),
  getters: {
    getContactById: (state) => (contactId) => {
      const profileStore = useProfileStore();
      if (contactId === profileStore.id)
        return {
          id: profileStore.id,
          name: profileStore.username,
          avatar: profileStore.avatar,
        };
      return state.contacts.find((contact) => contact.id === contactId);
    },
    getContactsInChannel: (state) => (channelId) => {
      const messagesStore = useMessagesStore();
      let contactsInChannel = [];
      messagesStore.messages
        .filter((message) => message.channelId === channelId)
        .map((message) => {
          if (
            !contactsInChannel.find((contact) => contact.id === message.author)
          ) {
            contactsInChannel.push(state.getContactById(message.author));
          }
        });
      return contactsInChannel;
    },
  },
});
