import useProfileStore from "@/stores/profile.js";

const login = async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        username: "l.simpson",
      });
    }, 2500);
  });
};

export const main = async () => {
  const profileStore = useProfileStore();
  const user = await login();
  profileStore.username = user.username;
  profileStore.status = "active";
};
